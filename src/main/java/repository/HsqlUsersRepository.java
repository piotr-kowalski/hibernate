package repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import UOW.HsqlUnitOfWork;
import UOW.UnitOfWorkRepository;
import cache.Cache;
import domain.Entity;
import domain.EntityState;
import domain.EnumerationValue;
import domain.User;

public class HsqlUsersRepository implements UserRepository, UnitOfWorkRepository {

	EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("myDatabase");
	EntityManager entityManager = entityManagerFactory.createEntityManager();

	public HsqlUsersRepository() {
		entityManager.getTransaction().begin();
	}

	public UserRepository withId(int id) {
		String query = String.format("FROM EnumerationValue WHERE id = %d", id);
		UserRepository userRepository = (UserRepository) entityManager.createQuery(query).getSingleResult();
		return userRepository;
	}

	public ArrayList<UserRepository> allOnPage(PagingInfo page) {
		ArrayList<UserRepository> enums = new ArrayList<UserRepository>();
		enums = (ArrayList<UserRepository>) entityManager.createQuery("FROM t_sys_users").getResultList();
		return enums;
	}

	public void add(UserRepository user) {
		entityManager.persist(user);
	}

	public void delete(UserRepository user) {
		entityManager.remove(user);

	}

	public void modify(UserRepository user) {
		entityManager.refresh(user);

	}

	public int count() {
		return (Integer) entityManager.createQuery("Select * From").getSingleResult();
	}

	public void persistAdd(UserRepository user) {
		entityManager.persist((UserRepository) user);
		entityManager.getTransaction().commit();

	}

	public void persistDelete(UserRepository user) {
		entityManager.remove((UserRepository) user);
		entityManager.getTransaction().commit();

	}

	public void persistUpdate(UserRepository user) {
		entityManager.refresh((UserRepository) user);
		entityManager.getTransaction().commit();

	}

	public ArrayList<UserRepository> withName(String name) {
		String query = String.format("FROM t_sys_users WHERE name = %s", name);
		ArrayList<UserRepository> userRepository = (ArrayList<UserRepository>) entityManager.createQuery(query)
				.getResultList();
		return userRepository;
	}

	public ArrayList<UserRepository> withIntKey(int key, String name) {
		String query = String.format("FROM t_sys_users WHERE intKey = %d and name = %s", key, name);
		ArrayList<UserRepository> userRepository = (ArrayList<UserRepository>) entityManager.createQuery(query)
				.getResultList();
		return userRepository;
	}

	public ArrayList<UserRepository> withStringKey(String key, String name) {
		String query = String.format("FROM t_sys_users WHERE  stringKey = key and name = %s", key, name);
		ArrayList<UserRepository> userRepository = (ArrayList<UserRepository>) entityManager.createQuery(query)
				.getResultList();
		return userRepository;
	}

	public void add(Object entity) {
		entityManager.persist(this);
		entityManager.getTransaction().commit();
	}

	public void delete(Object entity) {
	}

	public void modify(Object entity) {
		entityManager.persist(this);
		entityManager.getTransaction().commit();
	}

	public void persistAdd(Entity entity) {
		entityManager.persist(this);
		entityManager.getTransaction().commit();
	}

	public void persistDelete(Entity entity) {
		entityManager.persist(this);
		entityManager.getTransaction().commit();
	}

	public void persistUpdate(Entity entity) {
	}

	public User withLogin(String login) {
		return null;
	}

	public User withLoginAndPassword(String login, String password) {
		return null;
	}

	public void setupPermissions(User user) {
	}
}
