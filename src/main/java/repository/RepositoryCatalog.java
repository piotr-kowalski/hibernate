package repository;

public interface RepositoryCatalog {
	
	public EnumerationValueRepository enumerations();
	public UserRepository users();
}
