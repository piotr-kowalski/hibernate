package UOW;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import domain.Entity;
import domain.EntityState;

public class HsqlUnitOfWork implements UnitOfWork {

	private Connection connection;	
	private Map<Entity, UnitOfWorkRepository> entities= new LinkedHashMap<Entity, UnitOfWorkRepository>();
	
	public HsqlUnitOfWork () {}
	
	public HsqlUnitOfWork (Connection connection) {
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void saveChanges() {
		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Modified:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void undo() {
		entities.clear();
	}

	public void markAsNew(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.New);
		entities.put(entity, repo);
	}

	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repo);
	}

	public void markAsChanged(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.Modified);
		entities.put(entity, repo);
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Map<Entity, UnitOfWorkRepository> getEntities() {
		return entities;
	}

	public void setEntities(Map<Entity,UnitOfWorkRepository> entities) {
		this.entities = entities;
	}
	
	
}
