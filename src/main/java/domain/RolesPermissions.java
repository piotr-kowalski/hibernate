package domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "t_sys_permissions")
public class RolesPermissions extends Entity {

	@Column(name = "roleId")
	private int roleId;
	@Column(name = "permissionId")
	private int permissionId;

	public int getRoleId() { return roleId; }
	public void setRoleId(int roleId) { this.roleId = roleId; }

	public int getPermissionId() { return permissionId; }
	public void setPermissionId(int permissionId) { this.permissionId = permissionId; }
}
