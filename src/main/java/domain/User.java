package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@javax.persistence.Entity
@Table(name = "t_sys_user")
public class User extends Entity {

	@Column(name = "login", nullable = false, length = 30)
	private String login;
	@Column(name = "password", nullable = false, length = 30)
	private String password;
	@OneToOne
	@JoinColumn(name = "personId")
	private Person person;
	@OneToMany(mappedBy="RolesPermissions")
	@Enumerated(EnumType.STRING)
	private List <RolesPermissions> rolesPermissions = new ArrayList <RolesPermissions> ();
	@OneToMany(mappedBy="UserRoles")
	@Enumerated(EnumType.STRING)
	private List <UserRoles> userRoles = new ArrayList <UserRoles> ();
	
	public String getLogin() { return login; }
	public void setLogin(String login) { this.login = login; }
	
	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }
	
	public Person getPerson() { return person; }
	public void setPerson(Person person) { this.person = person; }
}
