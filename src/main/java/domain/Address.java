package domain;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "t_p_address")
public class Address extends Entity {

	@Column(name = "countryId")
	private int countryId;
	@Column(name = "regionId")
	private int regionId;
	@Column(name = "city")
	private String city;
	@Column(name = "street")
	private String street;
	@Column(name = "houseNumber")
	private String houseNumber;
	@Column(name = "localNumber")
	private String localNumber;
	@Column(name = "zipCode")
	private String zipCode;
	@Column(name = "typeId")
	private int typeId;

	public int getCountryId() { return countryId; }
	public void setCountryId(int countryId) { this.countryId = countryId; }

	public int getRegionId() { return regionId; }
	public void setRegionId(int regionId) { this.regionId = regionId; }

	public String getCity() { return city; }
	public void setCity(String city) { this.city = city; }

	public String getStreet() { return street; }
	public void setStreet(String street) { this.street = street; }

	public String getHouseNumber() { return houseNumber; }
	public void setHouseNumber(String houseNumber) { this.houseNumber = houseNumber; }

	public String getLocalNumber() { return localNumber; }
	public void setLocalNumber(String localNumber) { this.localNumber = localNumber; }

	public String getZipCode() { return zipCode; }
	public void setZipCode(String zipCode) { this.zipCode = zipCode; }

	public int getTypeId() { return typeId; }
	public void setTypeId(int typeId) { this.typeId = typeId; }
}
