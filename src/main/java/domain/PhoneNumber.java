package domain;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "t_p_phone")
public class PhoneNumber extends Entity {

	@Column(name = "phoneNumber")
	private String phoneNumber;
	@Column(name = "countryPrefix")
	private String countryPrefix;
	@Column(name = "cityPrefix")
	private String cityPrefix;
	@Column(name = "number")
	private String number;
	@Column(name = "typeId")
	private int typeId;

	public String getPhoneNumber() { return phoneNumber; }
	public void setPhoneNumber(String phoneNumber) { phoneNumber = phoneNumber; }
	
	public String getCountryPrefix() { return cityPrefix; }
	public void setCountryPrefix(String countryPrefix) { this.countryPrefix = countryPrefix; }
	
	public String getCityPrefix() { return cityPrefix; }
	public void setCityPrefix(String cityPrefix) { this.cityPrefix = cityPrefix; }

	public String getNumber() { return number; }
	public void setNumber(String number) { this.number = number; }

	public int getTypeId() { return typeId; }
	public void setTypeId(int typeId) { this.typeId = typeId; }
}
