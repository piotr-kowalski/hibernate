package domain;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "t_sys_enums")
public class EnumerationValue extends Entity {

	@Column(name = "intKey")
	private int intKey;
	@Column(name = "stringKey")
	private String stringKey;
	@Column(name = "value")
	private String value;
	@Column(name = "enumerationName")
	private String enumerationName;

	public int getIntKey() {
		return intKey; 
		}
	public void setIntKey(int intKey) {
		this.intKey = intKey; 
		}

	public String getStringKey() {
		return stringKey; 
		}
	public void setStringKey(String stringKey) {
		this.stringKey = stringKey; 
		}

	public String getValue() {
		return value; 
		}
	public void setValue(String value) {
		this.value = value; 
		}

	public String getEnumerationName() {
		return enumerationName;
		}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName; 
		}
}
