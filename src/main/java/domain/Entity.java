package domain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@javax.persistence.Entity
public abstract class Entity {

	@Id
	@GeneratedValue
	private int id;
	@Enumerated(EnumType.STRING)
	private EntityState state;

	public int getId() { 
		return id; 
		}
	public void setId(int id) {
		this.id = id; 
		}

	public EntityState getState() { 
		return state; 
		}
	public void setState(EntityState state) { 
		this.state = state; 
		}
}
