package domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@javax.persistence.Entity
@Table(name = "t_p_person")
public class Person extends Entity {

	@Column(name = "firstName")
	private String firstName;
	@Column(name = "surName")
	private String surName;
	@Column(name = "pesel")
	private String pesel;
	@Column(name = "nip")
	private String nip;
	@Column(name = "email")
	private String email;
	@Column(name = "dateOfBirth")
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	@Column(name = "address")
	@OneToMany(mappedBy = "address")
	private List <Address> address = new ArrayList <Address> ();
	@Column(name = "phoneNumber")
	@OneToMany(mappedBy = "phoneNumber")
	private List <PhoneNumber> phoneNumber = new ArrayList <PhoneNumber> ();
	
	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }

	public String getSurName() { return surName; }
	public void setSurName(String surName) { this.surName = surName; }

	public String getPesel() { return pesel; }
	public void setPesel(String pesel) { this.pesel = pesel; }

	public String getNip() { return nip; }
	public void setNip(String nip) { this.nip = nip; }

	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }

	public Date getDateOfBirth() { return dateOfBirth; }
	public void setDateOfBirth(Date dateOfBirth) { this.dateOfBirth = dateOfBirth; }

	public List<Address> getAddress() { return address; }
	public void setAddress(List<Address> address) { this.address = address; }
	
	public List<PhoneNumber> getPhoneNumber() { return phoneNumber; }
	public void setPhoneNumber(List<PhoneNumber> phoneNumber) { this.phoneNumber = phoneNumber; }
}
