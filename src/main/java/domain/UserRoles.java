package domain;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "t_sys_roles")
public class UserRoles extends Entity{

	@Column(name = "userId")
	private int userId;
	@Column(name = "roleId")
	private int roleId;
	
	public int getUserId() { return userId; }
	public void setUserId(int userId) { this.userId = userId; }
	
	public int getRoleId() { return roleId; }
	public void setRoleId(int roleId) { this.roleId = roleId; }
}
