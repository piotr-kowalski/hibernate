package cache;

import java.util.ArrayList;

import domain.EnumerationValue;

public class Updater implements Runnable {

	private static Updater instance;
	private Thread thread = new Thread(this);
	private long lifespan;
	private Cache cacheInstance;
	private ArrayList<EnumerationValue> latestData;

	private Updater() {

	}

	public static Updater getInstance() {
		if (instance == null)
			instance = new Updater();
		return instance;
	}

	public void run() {
		while (true && cacheInstance != null) {
			try {
				refreshCache();
				Thread.sleep(lifespan);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void refreshCache() {
		cacheInstance.clean();
		try {

			for (EnumerationValue enumValue : enumRepo.allOnPage(new PagingInfo())) {
				cacheInstance.put(enumValue);
			}

			Thread.sleep(2000);
			cacheInstance.finalizeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public long getLifespan() {
		return lifespan;
	}

	public void setLifespan(long lifespan) {
		this.lifespan = lifespan;
	}

	public Cache getCacheInstance() {
		return cacheInstance;
	}

	public void setCacheInstance(Cache cacheInstance) {
		this.cacheInstance = cacheInstance;
	}

	public ArrayList<EnumerationValue> getLatestData(boolean whichOne) {
		return latestData;
	}

	public void setLatestData(ArrayList<EnumerationValue> latestData) {
		this.latestData = latestData;
	}

	public void start() {
		thread.start();
	}
}
